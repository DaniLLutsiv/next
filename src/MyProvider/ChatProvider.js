import React, {useState} from 'react'
export const ChatContext = React.createContext()

export const ChatProvider = ({children}) => {
  const [newMessage, setNewMessage] = useState('')
  const [edit, setEdit] = useState(null)
  const [dataChat, setDataChat] = useState([])

  const [users, setUsers] = useState(['Den', 'Adolf228', 'Mashka', 'Nagibator', 'User'])

  // const changeNewMessage = (txt) => {
  //   setNewMessage(txt)
  // }

  const findElement = (id, data) => {
    for (let i = 0; i < data.length; i++) {
      if (data[i].id === id) {
        return i
      }
    }
  }

  const changeDataChat = (message, id) => {/// менять все смс или одно
    if (id || id === 0) {
      setDataChat(prevState => {
        const newState = [...prevState]
        newState[id] = {
          id: newState[id].id,
          author: newState[id].author,
          nick: newState[id].nick,
          time: newState[id].time,
          message: message,
          notes: newState[id].notes,
        }
        return newState
      })
      setEdit(null)
    } else {
      setDataChat(prevState => [...prevState, message])
    }
    setNewMessage('')
  }

  const changeNotes = (id) => {
    setDataChat(prevState => {
      const newState = [...prevState]
      newState[id] = {
        id: newState[id].id,
        author: newState[id].author,
        nick: newState[id].nick,
        time: newState[id].time,
        message: newState[id].message,
        notes: !newState[id].notes,
      }
      return newState
    })
  }

  const changeSmsText = (id) => {
    setNewMessage(dataChat[findElement(id, dataChat)].message)
  }

  const deleteMessage = (id) => {
    setDataChat(prevState => {
      return prevState.filter((el, index) => index !== id);
    })
  }

  return (
    <ChatContext.Provider value={{
      newMessage, dataChat, users, edit,
      changeNewMessage(txt) {
        setNewMessage(txt)
      },
      changeUsers(users) {
        setUsers(users)
      },
      setDataChat,
      deleteMessage,
      changeDataChat,
      changeNotes,
      changeSmsText,
      setEdit
    }}>
    {children}
  </ChatContext.Provider>
  )
}