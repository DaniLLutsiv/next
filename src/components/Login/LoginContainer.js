import {useContext, useEffect, useState} from 'react'
import Router from 'next/router'
import {errorLogin} from '../../errorHandler/errorLogin'
import {withAuthAccess} from '../Hoc/WithCheckAccess'
import {Api} from '../../../api/api'
import Login from './Login'
import {MyContext} from '../../MyProvider/MyProvider'
import {withAuthRedirect} from '../Hoc/withAuthRedirect'

const LoginContainer = () => {
  const state = useContext(MyContext)
  const [email, changeEmail] = useState('')
  const [statusLogin, setStatusLogin] = useState('')
  const [password, changePassword] = useState('')
  const [loader, changeLoader] = useState(false)

  function onVanishing() {
    changeEmail('')
    changePassword('')
  }

  function set() {
    changeLoader(true)
    Api.login(email, password)
    .then(response => {
      changeLoader(false)
      if (response.data.message === 'Auth') {
        localStorage.setItem('token', response.data.token)
        state.changeLogin(true)
      }
    })
    .catch((reason) => {
      debugger
      errorLogin(reason.response, state.setErrorRedirect, changeLoader, setStatusLogin, changePassword)
    })
  }
  useEffect(() => {
    if (state.login){
      Router.push('/')
    }
  },[state.login])

  return <>
    {state.login ?
        <>myRedirect</> :
        <Login onVanishing={onVanishing} changeEmail={changeEmail} changePassword={changePassword}
               set={set} status={state.status} statusLogin={statusLogin}
               email={email} password={password} loader={loader}/>}
  </>
}

export default withAuthAccess(LoginContainer)


