import React from 'react'
import MyInputs from '../Elements/MyInputs'
import MyButton from '../Elements/MyButton'
import LoginStatus from './Elements/LoginStatus'
import LoginLinks from './Elements/LoginLinks'
import LoginButtons from './Elements/LoginButtons'
import LoginHeader from './Elements/LoginHeader'
import Box from '@material-ui/core/Box'
import {useStyles} from './loginStyle'
import MyForm from '../Elements/MyForm/MyForm'
import {withAuthRedirect} from '../Hoc/withAuthRedirect'

const Login = (props) => {
  const classes = useStyles();
  return (
    <MyForm>
      <LoginHeader/>
      <Box className={classes.form} noValidate>
        <MyInputs label='Email' id='outlined-required'
                  type='Required' value={props.email}
                  onChange={props.changeEmail} status={props.statusLogin}/>

        <MyInputs label='Password' id='outlined-password-input'
                  type='password' helperText="incorrect email or password"
                  value={props.password} onChange={props.changePassword}
                  status={props.statusLogin}/>

        <MyButton view={props.email.length > 0 && props.password.length > 7}
                  onSubmit={props.set} text='Sing in'/>

        {props.status && <LoginStatus status={props.status}/>}

        <LoginLinks loader={props.loader}/>

        <LoginButtons onVanishing={props.onVanishing}/>
      </Box>
    </MyForm>
  )
}
export default Login