import React from 'react'
import Button from '@material-ui/core/Button'
import Box from '@material-ui/core/Box'
import Link from 'next/link'

const LoginButtons = (props) => {
  return (
      <Box m={1} p={1} display="flex" justifyContent='space-between'>
        <Button variant="contained" color="secondary" onClick={props.onVanishing}>Cancel</Button>
        <Button variant="contained" color="inherit">
          <Link href="/forgotPassword"><a>Forgot password?</a></Link>
        </Button>
      </Box>
  )
}
export default LoginButtons