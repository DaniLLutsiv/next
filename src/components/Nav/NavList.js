import React from 'react'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import HomeIcon from '@material-ui/icons/Home'
import ListItemText from '@material-ui/core/ListItemText'
import Divider from '@material-ui/core/Divider'
import Box from '@material-ui/core/Box'
import List from '@material-ui/core/List'
import ListNavPublicItems from './ListNavItems/ListNavPublicItems'
import ListNavPrivateItems from './ListNavItems/ListNavPrivateItems'
import Link from 'next/link'

const NavList = (props) => {
  return (
      <Box width={250} role="presentation"
          onClick={() => props.setState(false)}
          onKeyDown={() => props.setState(false)}>
        <List>
          <Link href="/">
            <a>
              <ListItem button>
                <ListItemIcon>
                  <HomeIcon/>
                </ListItemIcon>
                <ListItemText primary={'Home'}/>
              </ListItem>
            </a>
          </Link>

          <ListNavPublicItems/>
        </List>
        {props.access === 'admin' &&
          <>
            <Divider/>
            <List>
              <ListNavPrivateItems/>
            </List>
          </>
        }
      </Box>
  )
}

export default NavList