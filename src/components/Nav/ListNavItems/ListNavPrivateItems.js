import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import React from 'react'
import InboxIcon from '@material-ui/icons/MoveToInbox'
import MailIcon from '@material-ui/icons/Mail'
import Link from 'next/link'

const ListNavPrivateItems = () => {
  const elements =
      ['invite', 'users'].map((text, index) => (
        <Link href={text} key={text}>
          <a>
            <ListItem button>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon/> : <MailIcon/>}
              </ListItemIcon>
              <ListItemText primary={text}/>
            </ListItem>
          </a>
        </Link>
      ))

  return (
      <>
        {elements}
      </>
  )
}
export default ListNavPrivateItems