import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import PeopleIcon from '@material-ui/icons/People'
import ListItemText from '@material-ui/core/ListItemText'
import Link from 'next/link'


const ListNavPublicItems = () => {
  const elements =
      ['profile'].map((text, index) => (
      <Link href={text} key={text}>
        <a>
          <ListItem button>
            <ListItemIcon>
              <PeopleIcon/>
            </ListItemIcon>
            <ListItemText primary={text}/>
          </ListItem>
        </a>
      </Link>))

  return (
      <>
        {elements}
      </>
  )
}
export default ListNavPublicItems