import React from 'react'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'

const MyTeableHead = () => {
  return (
      <TableHead>
        <TableRow>
          <TableCell>_id</TableCell>
          <TableCell align="right">Nick</TableCell>
          <TableCell align="right">Email</TableCell>
          <TableCell align="right">Access</TableCell>
          <TableCell align="right">Reset</TableCell>
        </TableRow>
      </TableHead>
  )
}

export default MyTeableHead