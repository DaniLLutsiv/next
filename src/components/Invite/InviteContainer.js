import React, {useContext, useEffect, useState} from 'react'
import {MyContext} from '../../MyProvider/MyProvider'
import Invite from './Invite'
import {Api} from '../../../api/api'

const InviteContainer = () => {
  const state = useContext(MyContext)
  const [collection, setCollection] = useState([])
  const [email, changeEmail] = useState('')
  const [smsListen, setSmsListen] = useState('') // '' or 'yes' or 'no' or 'error'
  const [text, setText] = useState('incorrect email')
  const [reset, setReset] = useState('')

  async function addNewPeople(){
    setSmsListen('yes')
    let data = await Api.addNewPeople(email)

    if (data.data.message === 'ok') {
      setSmsListen('no')
      setTimeout(() => {setSmsListen('');changeEmail('');}, 3000)
    } else{
      setText(data.data.message)
      setSmsListen('error')
      setTimeout(() => setSmsListen(''), 3000)
    }
  }
  async function resetEmail(email){
    Api.apiRest(email).then((res) => {
      if (res.data.message === 'ok') {
        setReset(email)
        setTimeout(() =>  setReset(''),3000)
      }
    })
  }

  useEffect(() => {
    Api.invitations('invitations').then((collection) => {
      setCollection(collection.data.collection)
    })
  }, [smsListen])

  return <Invite login={state.login} access={state.access} collection={collection}
                 email={email} resetEmail={resetEmail} changeEmail={changeEmail}
                 smsListen={smsListen} setSmsListen={setSmsListen} text={text}
                 setText={setText} addNewPeople={addNewPeople} reset={reset}/>
}

export default InviteContainer

