import React, {useContext} from 'react'
import {MyContext} from '../../MyProvider/MyProvider'
import Router from 'next/router'

export const redirectWithError = (Component) => ((props) => {
  const state = useContext(MyContext)
  if (state.errorRedirect) {
    Router.push('/error')
  }
  return <Component {...props} />
})
