import React, {useContext, useEffect} from 'react'
import Router from 'next/router'
import {MyContext} from '../../MyProvider/MyProvider'
import Login from '../Login/Login'
import {Api} from '../../../api/api'

export const withAuthRedirect = (Component) => (props) => {
  const state = useContext(MyContext)

  useEffect(() => {
    if (!state.login){
      if (localStorage.getItem('token')){
        Api.apiData()
        .then((data) => {
          if (data.data.access === 'admin') {
            state.changeAccess('admin')
          } else {
            state.changeAccess('user')
          }
          state.changeOauth(data.data.oauth)
          state.changeEmail(data.data.email)
          state.changeNick(data.data.nick)
          state.changeLogin(true)
        })
        .catch((reason) => {
          Router.push('/login')
        })
      }else{
        Router.push('/login')
      }
    }
  },[state.login])

  return (
    <>
    {!state.login ?
        <div>Loading///</div> :
        <Component {...props} />}
    </>
  )

}

/*
function PrivatePage({ loggedIn, ...props }) {
  const state = useContext(MyContext)

  React.useEffect(() => {
    if (loggedIn) return;
    Router.replace("/private", "/login", { shallow: true });
  }, [loggedIn]);

  if (!loggedIn) return <LoginPage />;
  // the JSX the private page will render
}
*/
