import {useContext, useEffect, useState} from 'react'
import {Api} from '../../../api/api'
import {MyContext} from '../../MyProvider/MyProvider'
import Router from 'next/router'


export const withAuthAccess = (Component) => ((props) => {
  const state = useContext(MyContext)
  const [redirect, changeRedirect] = useState(false) // true or false if false then redirect

  useEffect(() => { /// если есть токен пошли проверочный запрос
    if (localStorage.getItem('token')) {// если ты залогинен то "/" иначе отобрази компоненту
      Api.apiData()
          .then((data) => {
            if (data.data.access === 'admin') {
              state.changeAccess('admin')
            } else {
              state.changeAccess('user')
            }
            state.changeOauth(data.data.oauth)
            state.changeLogin(true)
            Router.push('/')
      })
          .catch(() => {
            state.changeLogin(false)
            localStorage.removeItem('token')
            changeRedirect(true)
          })
    } else {
      changeRedirect(true)
    }
  }, [])

  return (
    <div>
      {redirect ? <Component {...props} /> : <></>}
    </div>
  )
})
