import React from 'react'
import { Container} from '@material-ui/core'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import {useStyles} from './MyFormStyle'

const MyForm = (props) => {
  const classes = useStyles();
  return (
      <Container maxWidth="sm">
        <Grid container className={classes.root}>
          <Grid item xs={12} sm={12} md={12} component={Paper} elevation={6} square>
            <div className={classes.paper}>
              {props.children}
            </div>
          </Grid>
        </Grid>
      </Container>
  )
}

export default MyForm