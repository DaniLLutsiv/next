import React from 'react'
import InputError from './InputError'
import Input from './Input'
import Box from '@material-ui/core/Box'

const MyInputs = (props) => {
  return <Box m={2}>
    {props.status ?
        <InputError label={props.label} value={props.value} helperText={props.helperText}/> :
        <Input value={props.value} onChange={props.onChange}
               type={props.type} label={props.label} id={props.id}/>}
  </Box>
}

export default MyInputs