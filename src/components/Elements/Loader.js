import React from 'react'
import CloseIcon from '@material-ui/icons/Close'
import CircularProgress from '@material-ui/core/CircularProgress'
import CheckIcon from '@material-ui/icons/Check'
import Box from '@material-ui/core/Box'

const Loader = (props) => {
  return <Box ml={2} m={1}>
    {props.smsListen === 'error' ? <Box mt={0.7}><CloseIcon color="secondary"/></Box> : props.smsListen === 'yes' ?
        <CircularProgress color="inherit"/> : props.smsListen === 'no' ? <Box mt={0.6}><CheckIcon style={{ color: 'green' }}/></Box> : false}
  </Box>
}

export default Loader