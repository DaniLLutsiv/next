import React from 'react'
import TextField from '@material-ui/core/TextField'

const Input = (props) => {
  return <TextField className="input"
                    fullWidth={true}
                    id={props.id}
                    label={props.label}
                    type={props.type}
                    variant="outlined"
                    value={props.value}
                    onChange={(e) => props.onChange(e.target.value)}/>
}

export default Input