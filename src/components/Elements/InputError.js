import React from 'react'
import TextField from '@material-ui/core/TextField'

/*<TextField
          error
          id="outlined-error-helper-text"
          label="Error"
          defaultValue="Hello World"
          helperText="Incorrect entry."
          variant="outlined"
        />*/


const InputError = (props) => {
  return <TextField
      fullWidth={true}
      error
      type={props.type}
      label={props.label}
      helperText={props.helperText}
      variant="outlined"
      value={props.value}/>
}

export default InputError