import React from 'react'
import Avatar from '@material-ui/core/Avatar'
import LockIcon from '@material-ui/icons/Lock'
import Typography from '@material-ui/core/Typography'

const MyHeaderForm = (props) => {
  return (
    <>
      <Avatar>
        <LockIcon />
      </Avatar>
      <Typography component="h1" variant="h5">
        {props.text}
      </Typography>
    </>
  )
}

export default MyHeaderForm