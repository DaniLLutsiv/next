import {useContext, useEffect, useState} from 'react'
import {Api} from '../../../api/api'
import {MyContext} from '../../MyProvider/MyProvider'
import Router from 'next/router'

const Oauth = () => {
  const state = useContext(MyContext)

  function processResponse(data){
    if (data.data.message === 'you auth'){
      localStorage.setItem('token', data.data.token)
    }
    state.changeLogin(true)
    Router.push('/')
  }

  function processReject(reason){
    state.changeStatus(reason.response.data.message)
    setTimeout(() => state.changeStatus('') ,3000)
    state.changeLogin(false)
    Router.push('/login')
  }
  useEffect(() => {
    const params = new URLSearchParams(document.location.search.substring(1)),
    code = params.get('code'),
    token = localStorage.getItem('token'),
    socialNetwork = document.location.href.includes('google') // гугл ли

    if (token){/// add account
      if (socialNetwork){
        Api.addOauthGoogle(code)
            .then(processResponse)
            .catch(processReject)
      }else{
        Api.addOauthFacebook(code)
            .then(processResponse)
            .catch(processReject)
      }
    }else{ /// login
      if (socialNetwork){
        Api.apiGoogle(code)
            .then(processResponse)
            .catch(processReject)
      }else{
        Api.apiFacebook(code)
            .then(processResponse)
            .catch(processReject)
      }
    }

  },[])

  return (
    <>
    </>
  )
}

export default Oauth

