import React, {useContext, useEffect, useState} from 'react'
import {MyContext} from '../../MyProvider/MyProvider'
import Users from './Users'
import {Api} from '../../../api/api'

const UsersContainer = () => {
  const state = useContext(MyContext)
  const [collection, setCollection] = useState([])
  const [rerender, setRerender] = useState(true)

  const changeLife = (e) => {
    Api.apiChangeLife(e).then((res) => {
      if (res.data.message === 'ok')
        setRerender(!rerender)
    })
  }

  useEffect(() => {
    Api.invitations('users').then((collection) => {
      setCollection(collection.data.collection)
    })
  }, [rerender])

  return <Users login={state.login} access={state.access} collection={collection} changeLife={changeLife}/>
}

export default UsersContainer



