import React from 'react'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import Button from '@material-ui/core/Button'
import TableBody from '@material-ui/core/TableBody'

const UsersTableBody = (props) => {
  return (
      <TableBody>
        {props.collection.map((row) => (<TableRow key={row._id}>
          <TableCell component="th" scope="row">{row._id}</TableCell>
          <TableCell align="right">{row.nick}</TableCell>
          <TableCell align="right">{row.email}</TableCell>
          <TableCell align="right">{row.access}</TableCell>
          <TableCell align="right">
            <Button variant="contained" color="primary"
                    onClick={() => props.changeLife(row.email)}>{row.life ? 'Delete' : 'Reset'}</Button>
          </TableCell>
        </TableRow>))}
      </TableBody>
  )
}

export default UsersTableBody