import React from 'react'
import Table from '@material-ui/core/Table'
import TableContainer from '@material-ui/core/TableContainer'
import Paper from '@material-ui/core/Paper'
import {Box} from '@material-ui/core'
import {withAuthRedirect} from '../Hoc/withAuthRedirect'
import {useStyles} from './UsersStyle'
import MyTeableHead from '../Invite/InviteElements/MyTeableHead'
import UsersTableBody from './UsersElements/UsersTableBody'

function Users(props) {
  const classes = useStyles()

  return (
    <div className={classes.wrap}>
      <Box m={5}>
        <div className={classes.list}>
          <TableContainer component={Paper}>
            <Table className={classes.table} size="small" aria-label="a dense table">
              <MyTeableHead/>
              <UsersTableBody collection={props.collection} changeLife={props.changeLife}/>
            </Table>
          </TableContainer>
        </div>
      </Box>
    </div>
  )
}

export default withAuthRedirect(Users)
