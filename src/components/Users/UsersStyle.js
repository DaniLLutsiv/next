import {makeStyles} from '@material-ui/core/styles'

export const useStyles = makeStyles({
  table: {
    minWidth: 600,
  }, wrap: {
    marginTop: '75px',
  }, list: {
    width: '100%',
  }, secondary: {
    light: '#6183ff', main: '#77af37', dark: '#64e32d', contrastText: '#ffffff',
  },
})