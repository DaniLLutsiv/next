import React from 'react'
import Box from '@material-ui/core/Box'
import IconButton from '@material-ui/core/IconButton'
import LinkOffIcon from '@material-ui/icons/LinkOff'
import LinkIcon from '@material-ui/icons/Link'

const ChatMessageNotes = (props) => {

  const onClickNotes = () => {
    props.changeNotes(props.id)
    props.setViewActions && props.setViewActions(null) /// для отображения под чатом
  }

  if(props.notes) {
    return (
      <Box mt={1}>
        <IconButton color="primary" onClick={onClickNotes}>
          <LinkOffIcon fontSize="small"/></IconButton>
      </Box>
    )
  }else{
    return (
      <Box mt={1}>
        <IconButton color="primary" onClick={onClickNotes}><LinkIcon fontSize="small"/></IconButton>
      </Box>
    )
  }
}

export default ChatMessageNotes