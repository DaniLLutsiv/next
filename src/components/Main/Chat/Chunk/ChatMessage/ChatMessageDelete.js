import React from 'react'
import Box from '@material-ui/core/Box'
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'

const ChatMessageDelete = (props) => {
  const onClickEdit = () => {
    props.actions(props.id)
    props.setViewActions(null)
  }
  return (
      <>
        {
          (props.actions && props.author === props.email) &&
          <Box mt={1}>
            <IconButton color="primary" onClick={onClickEdit}>
              <DeleteForeverIcon fontSize="small" />
            </IconButton>
          </Box>
        }
      </>
  )
}

export default ChatMessageDelete