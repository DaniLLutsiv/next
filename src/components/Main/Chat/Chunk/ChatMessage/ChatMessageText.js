import React from 'react'
import Box from '@material-ui/core/Box'
import ChatMessageNotes from './ChatMessageNotes'
import ChatMessageEdit from './ChatMessageEdit'
import Grid from '@material-ui/core/Grid'
import ChatMessageDelete from './ChatMessageDelete'
import MoreVertIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton'

const ChatMessageText = (props) => {
  return (
    <>
      <Grid item xs={12}>
        <Box display="flex" flexDirection={props.author === props.email ? 'row-reverse' : 'row'}>
          <Box m={1} p={1.5} component="span" bgcolor='#f5f5f5' borderRadius='10px' maxWidth='150px' boxShadow={4}>
            <span>{props.message}</span>
          </Box>
          {!props.actions ?
              <ChatMessageNotes id={props.id} notes={props.notes} changeNotes={props.changeNotes}
                                setViewActions={props.setViewActions}/> :


          props.viewActions === props.id || props.author !== props.email ? // провека на авторство и на выбраное сообщение
              <>
                <ChatMessageDelete actions={props.deleteMessageFromServer} author={props.author} id={props.id}
                                   email={props.email} setViewActions={props.setViewActions}/>

                <ChatMessageNotes id={props.id} notes={props.notes} changeNotes={props.changeNotes}
                                  setViewActions={props.setViewActions}/>

                <ChatMessageEdit actions={props.actions} author={props.author} id={props.id}
                                 email={props.email} changeSmsText={props.changeSmsText}
                                 setEdit={props.setEdit} setViewActions={props.setViewActions}/>
              </>:
              <Box mt={1}>
                <IconButton color="primary" onClick={() => {props.setViewActions(props.id)}}>
                  <MoreVertIcon fontSize="small" />
                </IconButton>
              </Box>
          }


        </Box>
      </Grid>
    </>
  )
}

export default ChatMessageText