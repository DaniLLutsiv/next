import React, {useContext, useEffect, useState} from 'react'
import Chat from './Chat'
import {ChatContext} from '../../../MyProvider/ChatProvider'
import {MyContext} from '../../../MyProvider/MyProvider'
import io from 'socket.io-client'

const ChatContainer = () =>{
  const chatState = useContext(ChatContext)
  const state = useContext(MyContext)
  const [viewActions, setViewActions] = useState(null)
  const socket = io.connect('ws://localhost:8080', {
    query: 'token=' + localStorage.getItem('token').slice(7),
    forceNew: true
  });


  useEffect(() => {
       socket.on('AllMessage', data => {
      chatState.setDataChat(data)
    })

    socket.on('editNotes', id => {
      chatState.changeNotes(id)
    })

    socket.on('editMessage', (message, id) => {
      chatState.changeDataChat(message, id)
    })

    socket.on('message', data => {
      chatState.changeDataChat(data)
    })

    socket.on('delete', id => {
      chatState.deleteMessage(id)
    })
  }, [])

  const setChangeNotesFromServer = (id) => {
    socket.emit('editNotes', id);
  }

  const deleteMessageFromServer = (id) => {
    socket.emit('delete', id);
  }

  const setDataFromServer = (email, nick, id) => {
    if (id || id === 0){
      socket.emit('editMessage', chatState.newMessage, id);
    }else{
      socket.send({
        author: email, nick: nick, message: chatState.newMessage, notes: false,
      });
    }
  }

  return <Chat setChangeNotesFromServer={setChangeNotesFromServer} setDataFromServer={setDataFromServer}
               viewActions={viewActions} setViewActions={setViewActions} deleteMessageFromServer={deleteMessageFromServer}
               setDataChat={chatState.setDataChat} edit={chatState.edit} setEdit={chatState.setEdit} email={state.email} nick={state.nick}
               changeNotes={chatState.changeNotes} value={chatState.newMessage} change={chatState.changeNewMessage}
               changeDataChat={chatState.changeDataChat}
               dataChat={chatState.dataChat} users={chatState.users} changeSmsText={chatState.changeSmsText}/>
}
export default ChatContainer