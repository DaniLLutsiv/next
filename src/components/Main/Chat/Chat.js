import React, {useEffect} from 'react'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import ChatUsers from './Chunk/ChatUsers'
import ChatMessage from './Chunk/ChatMessage/ChatMessage'
import ChatInput from './Chunk/ChatInput'


const Chat = (props) => {

  return (
    <Box>
      <Grid container>
        <Grid item xs={12}>
          <Typography variant="h5" align="center">Online chat</Typography>
        </Grid>
      </Grid>
      <Grid container component={Paper} height='90vh'>
        <ChatUsers users={props.users}/>

        <ChatMessage dataChat={props.dataChat} email={props.email}
                     deleteMessageFromServer={props.deleteMessageFromServer}
                     viewActions={props.viewActions} setViewActions={props.setViewActions}
                     changeNotes={props.setChangeNotesFromServer} changeSmsText={props.changeSmsText}
                     setEdit={props.setEdit} actions={true}/>

        <ChatInput value={props.value} change={props.change}
                   changeDataChat={props.setDataFromServer} email={props.email}
                   nick={props.nick} edit={props.edit}/>
      </Grid>
    </Box>
  )
}


export default Chat




//import io from 'socket.io-client'
//import {useState} from "react"
//var socket = new WebSocket("ws://172.17.0.3:8080");
//var socket = new WebSocket("ws://localhost:3000");
//const socket = openSocket('ws://localhost:8080')

//let socket = new WebSocket("ws://localhost:3000");





/*class Chat extends React.Component{
  constructor(props) {
    super(props)
    this.socket = new WebSocket("ws://172.17.0.3:8080");
  }
  componentDidMount() {
    this.socket.onmessage = function(event) {
      console.log(event.data)
    };
    this.socket.onclose = function(event) {
      if (event.wasClean) {
        alert(`[close] Соединение закрыто чисто, код=${event.code} причина=${event.reason}`);
      } else {
        // например, сервер убил процесс или сеть недоступна
        // обычно в этом случае event.code 1006
        alert('[close] Соединение прервано');
      }
    };

    this.socket.onerror = function(error) {
      alert(`[error] ${error.message}`);
    };
  }


  send = () => {
    this.socket.send("Меня зовут Джон");
  }
  render(){
    return(
        <>
          chat
          {/!*<input value={value} onChange={(e) => setValue(e.target.value)}/>*!/}
          <button onClick={this.send}>!!!</button>
          {/!*{txt.map((html, index) => <div key={index}>{html}</div>)}*!/}
        </>
    )
  }
}

export default Chat


function Chat() {
  const [value, setValue] = useState('')
  const [txt, setTxt] = useState([])



  const click = () => {
    debugger
    socket.send('Hello');
  }
  return (
      <>
        <input value={value} onChange={(e) => setValue(e.target.value)}/>
        <button onClick={click}></button>
        {txt.map((html, index) => <div key={index}>{html}</div>)}
      </>
  )
}

export default Chat*/


