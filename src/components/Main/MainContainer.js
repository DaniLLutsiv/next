import React, {useContext, useEffect, useState} from 'react'
import {MyContext} from '../../MyProvider/MyProvider'
import Main from './Main'
import {withAuthRedirect} from '../Hoc/withAuthRedirect'
import {errorHandler} from '../../errorHandler/errorHandler'
import {Api} from '../../../api/api'

const MainContainer = () => {
  const state = useContext(MyContext)
  const [selectedItem, setSelectedItem] = useState(0)

  const onHandleChange = (event, newValue) => {
    setSelectedItem(newValue)
  }

  useEffect(() => {
    Api.apiData()
      .then((data) => {
        if (data.data.access === 'admin') {
          state.changeAccess('admin')
        } else {
          state.changeAccess('user')
        }
        state.changeOauth(data.data.oauth)
        state.changeEmail(data.data.email)
        state.changeNick(data.data.nick)
      })
      .catch((reason) => {
        errorHandler(reason.response, state)
      })
  }, [state.login])

  return <Main login={state.login} selectedItem={selectedItem} onHandleChange={onHandleChange}/>
}

export default withAuthRedirect(MainContainer)

