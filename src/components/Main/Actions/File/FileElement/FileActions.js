import React from 'react'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'

const FileActions = (props) => {
  return (
    <Box mt={6} m={6} display="flex">
      <Box mr={2}>
        <Box display="none">
          <input
              ref={props.refInput}
              id="contained-button-file"
              multiple
              type="file"
          />
        </Box>
        <label htmlFor="contained-button-file">
          <Button variant="contained" color="primary" component="span">
            Upload File
          </Button>
        </label>
      </Box>

      <Button variant="contained" onClick={props.setFile}>
        Send
      </Button>
    </Box>
  )
}

export default FileActions