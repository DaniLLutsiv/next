import React from 'react';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'
import MyForm from '../../../../Elements/MyForm/MyForm'
import FormDialog from './FormDialog'

function handleClick(event) {
  event.preventDefault();
  console.info('You clicked a breadcrumb.');
}

const Breadcrumb = (props) => {
  return (
    <Grid container>
      <Grid item xs={10}>
        <Box border={1} p={1} >
          <Breadcrumbs aria-label="breadcrumb">
            <div color="inherit" onClick={() => {props.setPath([])}}>
              /home
            </div>

            {props.path.map((el, index) => {
              return (
                <div color="inherit" onClick={() => {props.setPath( props.path.slice(0, index+1) )}} key={el}>
                  {el}
                </div>
              )
            })}

          </Breadcrumbs>
        </Box>
      </Grid>

      <Grid item xs={2}>
        <FormDialog addDir={props.addDir}/>
      </Grid>
    </Grid>
  )
}

export default Breadcrumb