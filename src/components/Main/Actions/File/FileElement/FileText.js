import React from 'react'
import {config} from '../../../../../../config'
import Typography from '@material-ui/core/Typography'

const FileText = (props) => {
  return (
      <Typography variant="h6" align="center">
        <a href={config.serverUrl+'filesChat/'+ props.path.join('/') + '/' + props.text}>
          {props.text}
        </a>
      </Typography>
  )
}

export default FileText