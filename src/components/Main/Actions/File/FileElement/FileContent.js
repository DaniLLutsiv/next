import React from 'react'
import Box from '@material-ui/core/Box'
import FolderIcon from '@material-ui/icons/Folder'
import Typography from '@material-ui/core/Typography'
import DescriptionIcon from '@material-ui/icons/Description'
import FileText from './FileText'
import GridList from '@material-ui/core/GridList'
import {useStyles} from '../FileStyle'

const FileContent = (props) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <GridList cellHeight={100} className={classes.gridList} cols={4}>
        { props.content.map((el, index) => {
          return (
              <Box key={index} mr={3.4}>
                {typeof el === 'object' ?
                    <Box textAlign="center">
                      <FolderIcon fontSize="large" />
                      <Typography variant="h6" align="center">
                        <div onClick={() => props.setPath(props.path.concat(el.name))}>
                          {el.name}
                        </div>
                      </Typography>
                    </Box>:
                    <Box textAlign="center">
                      <DescriptionIcon fontSize="large"/>
                      <FileText text={el} path={props.path}/>
                    </Box>
                }
              </Box>
          )
        })}
      </GridList>
    </div>
  )
}

export default FileContent