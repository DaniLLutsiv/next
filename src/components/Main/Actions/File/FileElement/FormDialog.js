import React from 'react'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Box from '@material-ui/core/Box'


/*

class FileList extends React.Component {
  state = {
    files: [
      'nice.pdf',
      'verycool.jpg',
      'amazing.png',
      'goodstuff.mp3',
      'thankyou.doc'
    ]
  }
  handleDrop = (files) => {
    let fileList = this.state.files
    for (var i = 0; i < files.length; i++) {
      if (!files[i].name) return
      fileList.push(files[i].name)
    }
    this.setState({files: fileList})
  }
  render() {
    return (
        <FormDialog handleDrop={this.handleDrop}>
          <div style={{height: 300, width: 250}}>
            {this.state.files.map((file,i) =>
                <div key={i}>{file}</div>
            )}
          </div>
        </FormDialog>
    )
  }
}
export default FileList
*/


const FormDialog = (props) => {
  const [open, setOpen] = React.useState(false)
  const [value, setValue] = React.useState('')

  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
    setValue('')
  }

  return (
      <Box>
        <Button variant="outlined" color="primary" onClick={handleClickOpen}>
          <Box height='26px'pt={0.5}>
            + dir
          </Box>
        </Button>
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Add directory</DialogTitle>
          <DialogContent>
            <TextField
                value={value}
                autoFocus
                margin="dense"
                label="Nane diretory"
                type="email"
                fullWidth
                onChange={(e) => setValue(e.target.value)}/>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={() => {handleClose(); props.addDir(value)}} color="primary">
              Add
            </Button>
          </DialogActions>
        </Dialog>
      </Box>
  )
}

export default FormDialog
