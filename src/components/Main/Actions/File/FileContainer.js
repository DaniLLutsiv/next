import React, {useEffect, useRef, useState} from 'react'
import File from './File'
import {Api} from '../../../../../api/api'

const FileContainer = () => {
  const fileInput = useRef();
  const [files, setFiles] = useState({content:[]})
  const [path, setPath] = useState([])
  const [openError, setOpenError] = React.useState(false);

  useEffect(() => {
    getFilesFromServer()
  }, [])

  let newObj = files
  path.forEach(path => {
    newObj = newObj.content.find(el => el.name === path)
  })

  const getFilesFromServer = () => {
    Api.apiFiles().then((res) => {
      setFiles(res.data)
    })
  }

  const setFile = (file) => {
    const formData = new FormData()
    let fileName = ''
    if (file){
      formData.append('file',file)
      fileName = file.name
    }else if(fileInput.current.files[0]){
      formData.append('file', fileInput.current.files[0])
      fileName = fileInput.current.files[0]
    }
    Api.apiSendLogo(formData, fileName, 'filesChat/' + path.join('/') )
      .then((res) => {
         if (res.data.message === 'ok'){
           getFilesFromServer()
         }
      })
  }


  const addDir = (name) => {

    Api.addDir(path.join('/') + '/' + name)
      .then((res) => {
        if (res.data.message === 'ok'){
          getFilesFromServer()
        }
      })
      .catch(() => {
        setOpenError(true)
      })
  }

  return (
      <File refInput={fileInput} files={newObj} setFile={setFile} path={path} setPath={setPath} addDir={addDir}
            openError={openError} setOpenError={setOpenError}/>
  )
}

export default FileContainer