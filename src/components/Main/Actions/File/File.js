import React from 'react'
import Box from '@material-ui/core/Box'
import Breadcrumb from './FileElement/BreadCrumbs'
import MyForm from '../../../Elements/MyForm/MyForm'
import FileContent from './FileElement/FileContent'
import FileActions from './FileElement/FileActions'
import MuiAlert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar'
import Button from '@material-ui/core/Button'
import Alert from '@material-ui/lab/Alert'
import makeStyles from '@material-ui/core/styles/makeStyles'
import FileList from './FileElement/FilesDrag'


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

const File = (props) => {
  const classes = useStyles();

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    props.setOpenError(false)
  }
  const hendleDropFile = (ev) => {
    ev.preventDefault()
    if (ev.dataTransfer.files[0]){
      props.setFile(ev.dataTransfer.files[0])
    }
  }

  return (
    <Box mt={-20} onDrop={hendleDropFile} onDragOver={ev => ev.preventDefault()}>
      <MyForm>
        <Box width="100%" height='500px'>
          <Breadcrumb path={props.path} setPath={props.setPath} addDir={props.addDir}/>

          <FileContent content={props.files.content} setPath={props.setPath} path={props.path}/>

          <FileActions refInput={props.refInput} setFile={props.setFile}/>
        </Box>
      </MyForm>

      {/*  Всплываха с ошибкой  */}
      <div className={classes.root}>
        <Snackbar open={props.openError} autoHideDuration={3000} onClose={handleClose}>
          <Alert onClose={handleClose} severity="error">
           Error add dir
          </Alert>
        </Snackbar>
      </div>


    </Box>
  )
}

export default File