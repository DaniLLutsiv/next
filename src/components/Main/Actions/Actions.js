import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import FileContainer from './File/FileContainer'
import Notes from './Notes'

const Actions = props => {
  return (
    <>
      <AppBar position="relative">
        <Tabs value={props.selectedItem} onChange={props.onHandleChange} variant="fullWidth" aria-label="simple tabs example">
          <Tab label="File" fullWidth="250" />
          <Tab label="Notes" value={1}/>
        </Tabs>
      </AppBar>
      {props.selectedItem === 0 ? <FileContainer/>:<Notes/>}
    </>
  )
}
export default Actions