import {useState} from 'react'
import CompleteMarkup from './CompleteMarkup'
import setCICR from './setCICR'
import {withAuthAccess} from '../Hoc/WithCheckAccess'

const CompleteMarkupContainer = (props) => { // рендер компоненты CompleteMarkupContainer или CompleteRecoverContainer
  const [statusInvitation, setStatusInvitation] = useState('') // text
  const [password1, changePassword1] = useState('') // text
  const [password2, changePassword2] = useState('') // text
  const [smsListen, setSmsListen] = useState('') // '' or 'yes' or 'no' or 'error'

  const set = () => {
      setCICR(password1, password2,
          props.slice, props.Api,
          changePassword2,
          setStatusInvitation, setSmsListen)
  }

  return (
    <CompleteMarkup password1={password1} password2={password2}
                    changePassword1={changePassword1} changePassword2={changePassword2}
                    set={set} smsListen={smsListen}
                    statusInvitation={statusInvitation} buttonText={props.buttonText}/>
  )
}

export default withAuthAccess(CompleteMarkupContainer)