import Router from 'next/router'

const setCICR = (password1, password2, slice, Api, changePassword2, setStatusInvitation, setSmsListen) => {
  if (password1 === password2) {
    setSmsListen('yes')
    Api(password1, document.location.href.slice(slice)).then((data) => {
      if (data.data.message === 'ok') {
        setSmsListen('no')

        setTimeout(() => Router.push('/'), 3000)

      } else if(data.data.message === 'Enter a password of at least 8 characters'){
        setSmsListen('error')
        setStatusInvitation(data.data.message)
        setTimeout(() => setStatusInvitation(''), 3000)
      }else{
        setSmsListen('error')
        setStatusInvitation(data.data.message)
        setTimeout(() => Router.push('/'), 3000)
      }
    })
  } else {
    setStatusInvitation('PASSWORDS DID NOT MATCH')
    setSmsListen('error')
    setTimeout(() => {setStatusInvitation(''); changePassword2('');setSmsListen('')}, 3000)
  }
}

export default setCICR