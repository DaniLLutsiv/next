import {makeStyles} from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%', maxWidth: 360, backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  large: {
    margin: 'auto',
    width: theme.spacing(30),
    height: theme.spacing(30),
  },
  nick: {
    margin: '10px',
    border: '2px solid #dedcdc',
    padding: theme.spacing(1),
  },
}))
