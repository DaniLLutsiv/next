import React from 'react'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import PeopleIcon from '@material-ui/icons/People'
import ListItemText from '@material-ui/core/ListItemText'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import Collapse from '@material-ui/core/Collapse'
import {useStyles} from '../ProfileStyle'
import ProfileFile from './ProfilrFile'

const ProfileLogo = (props) => {
  const classes = useStyles()

  return (
      <>
        <ListItem button onClick={() => props.setOpen3(!props.open3)}>
          <ListItemIcon>
            <PeopleIcon/>
          </ListItemIcon>
          <ListItemText primary="Add logo"/>
          {props.open3 ? <ExpandLess/> : <ExpandMore/>}
        </ListItem>

        <Collapse in={props.open3} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItem button className={classes.nested}>
              <ProfileFile addLogo={props.addLogo} setOpen3={props.setOpen3}/>
            </ListItem>
          </List>
        </Collapse>
      </>
  )
}

export default ProfileLogo