import React from 'react'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import Collapse from '@material-ui/core/Collapse'
import {useStyles} from '../ProfileStyle'
import ShortTextIcon from '@material-ui/icons/ShortText'
import Input from '../../Elements/Input'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'

const ProfileNick = (props) => {
  const classes = useStyles()

  return (
      <>
        <ListItem button onClick={() => props.setOpen2(!props.open2)}>
          <ListItemIcon>
            <ShortTextIcon/>
          </ListItemIcon>
          <ListItemText primary="Change nick"/>
          {props.err && props.err}
          {props.open2 ? <ExpandLess/> : <ExpandMore/>}
        </ListItem>

        <Collapse in={props.open2} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItem button className={classes.nested}>
              <Input value={props.nick} onChange={props.setNick}
                     type="Required" label={'Nickname'} id="outlined-required"/>
              <Box m={1}>
                <Button variant="contained" color="primary" onClick={
                  () => {props.addNick();
                    props.setOpen2(false)
                  }}>+</Button>
              </Box>
            </ListItem>
          </List>
        </Collapse>
      </>
  )
}

export default ProfileNick