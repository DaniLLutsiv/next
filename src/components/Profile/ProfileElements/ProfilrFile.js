import React from 'react';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box'

const ProfileFile = (props) => {
  const fileInput = React.useRef();

  return (
      <Box m={1} display="flex">
        <Box mr={2}>
          <Box display="none">
            <input
                ref={fileInput}
                id="contained-button-file"
                multiple
                type="file"
            />
          </Box>
          <label htmlFor="contained-button-file">
            <Button variant="contained" color="primary" component="span">
              Upload File
            </Button>
          </label>
        </Box>

        <Button variant="contained" onClick={() => {
          props.addLogo(fileInput.current.files[0]);
          props.setOpen3(false)
        }}>Send</Button>
      </Box>
  )
}

export default ProfileFile