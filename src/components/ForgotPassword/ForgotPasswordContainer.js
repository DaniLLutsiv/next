import React, {useContext, useState} from 'react'
import ForgotPassword from './ForgotPassword'
import setFP from './setFP'
import {withAuthAccess} from '../Hoc/WithCheckAccess'
import {MyContext} from '../../MyProvider/MyProvider'

const ForgotPasswordContainer = (props) => {
  const state = useContext(MyContext)
  const [statusForgot, setStatusForgot] = useState('')
  const [email, changeEmail] = useState('')
  const [smsListen, setSmsListen] = useState('') // '' or 'yes' or 'no' or 'error'

  const set = () => {
    setFP(email, props.Api, setStatusForgot, setSmsListen, changeEmail, state)
  }

  return (
    <ForgotPassword email={email} changeEmail={changeEmail} set={set}
                            statusForgot={statusForgot} smsListen={smsListen}/>
  )

}

export default withAuthAccess(ForgotPasswordContainer)
