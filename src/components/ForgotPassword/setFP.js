import {errorHandler} from '../../errorHandler/errorHandler'
import Router from 'next/router'

const setFP = (email, Api, setStatusForgot, setSmsListen, changeEmail, state) => {
    setSmsListen('yes')
    Api(email)
        .then((data) => {
          setTimeout(() => {setSmsListen(''); setStatusForgot('')}, 3000)
          if (data.data.message === 'ok') {
            setSmsListen('no')
            setStatusForgot(`Sms listen in email ${email}`)
            setTimeout(() => Router.push('/'), 3000)
          }
        })
        .catch((reason) => {
          setTimeout(() => {setSmsListen(''); setStatusForgot('')}, 3000)
          setSmsListen('error')
          setStatusForgot(reason.response.data.message)
          changeEmail('')
          errorHandler(reason.response, state)
        })
}

export default setFP