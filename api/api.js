import axios from 'axios'
import {config} from '../config'

const instance = axios.create({
  baseURL: config.serverUrl,
  headers: {
    'Content-Type': 'application/json',
    //'Authorization': localStorage.getItem('token'),
  },
})

export const Api = {
  login(email, password) {
    return instance.post(`api/login`, JSON.stringify({email, password}))
  },
  apiFiles(){
    return instance.get('/api/files', {headers: {'Authorization': localStorage.getItem('token')}})
  },
  apiData() {
    return instance.get(`api/data`, {headers: {'Authorization': localStorage.getItem('token')}})
  },
  addNewPeople(email) {
    return instance.post(`api/invite`, JSON.stringify({email}),{headers: {'Authorization': localStorage.getItem('token')}})
  },
  apiGoogle(code) {
    return instance.post(`api/oauth-google`, JSON.stringify({code}))
  },
  apiFacebook(code) {
    return instance.post(`api/oauth-facebook`, JSON.stringify({code}))
  },
  addOauthGoogle(code){
    return instance.post('api/addOauthGoogle', JSON.stringify({code}), {headers: {'Authorization': localStorage.getItem('token')}})
  },
  addOauthFacebook(code){
    return instance.post('api/addOauthFacebook', JSON.stringify({code}), {headers: {'Authorization': localStorage.getItem('token')}})
  },
  apiForgotPassword(email){
    return instance.post(`api/forgot-password`, JSON.stringify({email: email}))
  },
  apiCompleterecover(password, hash){
    return instance.post(`api/complete-recover`, JSON.stringify({password, hash}))
  },
  apiCompleteInvitation(password, hash){
    return instance.post(`api/complete-invitation`, JSON.stringify({password, hash}))
  },
  invitations(collection) {
    return instance.post(`api/collection`,  JSON.stringify({collection}), {headers: {'Authorization': localStorage.getItem('token')}})
  },
  apiAddNick(nick){
    return instance.post(`api/add-nick`, JSON.stringify({nick}),{headers: {'Authorization': localStorage.getItem('token')}})
  },
  apiRest(email) {
    return instance.post(`api/reset`, JSON.stringify({email}), {headers: {'Authorization': localStorage.getItem('token')}})
  },
  apiSendLogo(formData, name, url){
    return instance.post(`api/add-file`, formData,{headers: {'Authorization': localStorage.getItem('token'),'Name': name, 'FileUrl': url,
        'Content-Type': 'multipart/form-data'}})
  },
  apiChangeLife(email){
    return instance.post(`api/change-life`, JSON.stringify({email}),{headers: {'Authorization': localStorage.getItem('token')}})
  },
  addDir(name){
    return instance.post(`api/add-dir`, JSON.stringify({name}),{headers: {'Authorization': localStorage.getItem('token')}})
  }
}