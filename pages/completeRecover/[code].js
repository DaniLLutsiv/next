import CompleteMarkupContainer from '../../src/components/CompleteMarkup/CompleteMarkupContainer'
import {Api} from '../../api/api'

function code() {
  return (
    <CompleteMarkupContainer Api={Api.apiCompleterecover} slice="38" buttonText="Change password" />
  )
}

export default code
