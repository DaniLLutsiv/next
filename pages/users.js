import {withAuthRedirect} from '../src/components/Hoc/withAuthRedirect'
import Nav from '../src/components/Nav/Nav'
import UsersContainer from '../src/components/Users/UserContainer'

function users() {
  return (
      <>
        <Nav/>
        <UsersContainer/>
      </>
  )
}

export default withAuthRedirect(users)
