import {withAuthRedirect} from '../src/components/Hoc/withAuthRedirect'
import Nav from '../src/components/Nav/Nav'
import InviteContainer from '../src/components/Invite/InviteContainer'

function invite() {
  return (
      <>
        <Nav/>
        <InviteContainer/>
      </>
  )
}

export default withAuthRedirect(invite)
