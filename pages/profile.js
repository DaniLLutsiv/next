import {withAuthRedirect} from '../src/components/Hoc/withAuthRedirect'
import Nav from '../src/components/Nav/Nav'
import ProfileContainer from '../src/components/Profile/ProfileContainer'

function Home() {
  return (
    <>
      <Nav/>
      <ProfileContainer/>
    </>
  )
}

export default withAuthRedirect(Home)
