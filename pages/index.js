import {withAuthRedirect} from '../src/components/Hoc/withAuthRedirect'
import Nav from '../src/components/Nav/Nav'
import MainContainer from '../src/components/Main/MainContainer'

function Home() {
  return (
    <>
      <Nav/>
      <MainContainer />
    </>
  )
}

export default withAuthRedirect(Home)
