import ForgotPasswordContainer from '../src/components/ForgotPassword/ForgotPasswordContainer'
import {Api} from '../api/api'

function forgotPassword() {
  return (
      <>
        <ForgotPasswordContainer Api={Api.apiForgotPassword}/>
      </>
  )
}

export default forgotPassword
