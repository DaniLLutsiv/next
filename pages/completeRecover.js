import {withAuthRedirect} from '../src/components/Hoc/withAuthRedirect'
import CompleteMarkupContainer from '../src/components/CompleteMarkup/CompleteMarkupContainer'
import {Api} from '../api/api'

function completeRecover() {
  return (
    <CompleteMarkupContainer Api={Api.apiCompleterecover} slice="38" buttonText="Change password" />
  )
}

export default completeRecover
