import CompleteMarkupContainer from '../../src/components/CompleteMarkup/CompleteMarkupContainer'
import {Api} from '../../api/api'

function code() {
  return (
      <CompleteMarkupContainer Api={Api.apiCompleteInvitation} slice="41" buttonText="Add account" />
  )
}

export default code
